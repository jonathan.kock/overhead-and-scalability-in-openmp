#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <stdbool.h>

#include "print.h"
#include "benchmarks.h"
#include "thesis_math.h"
#include "tasks.h"

long reps;
int iterations;
char *path;
char *options;
char *name_ext;
bool bench_for;
bool bench_sync;
bool bench_task;
bool bench_loop;
bool bench_weak;
bool bench_strong;
bool bench_hints;

int parse_args(int, char **);
int execute(void);
int benchmark(char *, void (*)(long), bool);
int reference(char *, void (*)(long));
double* measure(void (*)(long));
int main(int , char **);
