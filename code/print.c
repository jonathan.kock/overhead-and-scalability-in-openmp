#include <stdio.h>
#include <time.h>

#include "print.h"

FILE *results;
FILE *raw;
char time_buffer[40];

/* create two files at desired location, one containing raw data, the other results" */
int print_setup(char *path, char *options, int threads_number, int iterations, long task_reps, long reps, char *name_ext) {
    time_t now;
    struct tm *time_tm;
    time(&now);
    time_tm = localtime( &now);
    strftime(time_buffer,40,"%d-%m-%y_%H-%M-%S",time_tm);
    char file_path[1024];
    snprintf(file_path,1024,"%s/oas_%s_%s_%d_%li-%li_%s.csv",path,name_ext,options,threads_number,task_reps,reps,time_buffer);
    results = fopen(file_path,"w+");
    snprintf(file_path,1024,"%s/raw_oas_%s_%s_%d_%li-%li_%s.csv",path,name_ext,options,threads_number,task_reps,reps,time_buffer);
    raw = fopen(file_path,"w+");
    fprintf(results,"Test executed at %s, parameters %s, threads %d, iterations %d, non splittable workload %li, repititions %li\n", 
            time_buffer, options, threads_number, iterations, task_reps, reps);
    fprintf(results,"test name, mean, stddev, 95CONF+-, overhead, overhead 95CONF+-, overhead ratio\n");
    fprintf(raw,"Test executed at %s, parameters %s, threads %d, iterations %d, non splittable workload %li, repititions %li\n", 
            time_buffer, options, threads_number, iterations, task_reps, reps);
    return 0;
}

/* prints the int array to raw and starts a new line, seperates each entry with ',' */
int print_int(int size, int *toPrint) {
    int i;
    if(size==0) {
        return 0;
    }
    for(i = 0; i < size-1; i++) {
        fprintf(raw,"%d, ",toPrint[i]);
    }
    fprintf(raw,"%d\n", toPrint[size-1]);
    return 0;
}

/* prints the string to raw and starts a new line*/
int print_string(char *toPrint) {
    fprintf(raw,"%s\n",toPrint);
    return 0;
}

/* prints the double array to raw and starts a new line, seperates each entry with ',' */
int print_doubles(int size, double *toPrint) {
    int i;
    if(size==0) {
        return 0;
    }
    for(i = 0; i < size-1; i++) {
        fprintf(raw,"%f, ",toPrint[i]);
    }
    fprintf(raw,"%f\n", toPrint[size-1]);
    return 0;
}

int print_result(int size, double *result_array, char *name) {
    int i;
    fprintf(results,"%s, ",name);
    if(size==0) {
        fprintf(results,"\n");
        return 0;
    }
    for(i = 0; i < size - 1; i++) {
        fprintf(results,"%f, ",result_array[i]);
    }
    fprintf(results,"%f\n",result_array[size-1]);
    return 0;
}

/* closes the file pointers */
int print_close() {
    fclose(results);
    fclose(raw);
    return 0;
}
