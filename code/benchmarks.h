#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "tasks.h"

extern int threads_number;
extern int info;
extern int chunksize;
extern double task_mean;
extern double task_stddev;
extern omp_lock_t lock;
extern int array_size;


double getsetRefMean(void (*) (long),double);

double getsetRefStddev(void (*) (long),double);

void reference_task(long);

void for_reference(long reps);

void for_weak_static(long);

void for_strong_static(long);

void for_weak_dynamic(long);

void for_strong_dynamic(long);

void task_weak(long reps);

void task_strong(long reps);

void task_loop_weak(long reps);

void task_loop_strong(long reps);

void array_reference(long);

void barrier_bench(long);

void single_bench(long);

void critical_bench(long);

void lock_bench(long);

void order_bench(long);

void atomic_bench(long);

void reduction_bench(long);

void n_hints(long);

void u_hints(long);

void s_hints(long);

void c_hints(long);

void u_n_hints(long);

void c_n_hints(long);

void u_s_hints(long);

void c_s_hints(long);
