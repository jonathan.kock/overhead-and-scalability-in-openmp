#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "thesis_math.h"
#include "print.h"

int compare(const void * a, const void * b) {
    return ( *(double*)a - *(double*)b );
}

/*
calculates mean, stddev, 95% conf +- interval, returns them as a double array of size 3
*/
double * do_math(int size, double *results) {
    double * calc = malloc(sizeof(double)*3);
    double tmp;
    int i;
    calc[0] = 0.0;
    for(i = 0; i < size; i++) {
        calc[0] += results[i];
    }
    calc[0] /= size;
    calc[1] = 0.0;
    for(i = 0; i < size; i++) {
        tmp = results[i] - calc[0];
        calc[1] += tmp * tmp;
    }
    calc[1] /= size;
    calc[1] = sqrt(calc[1]);
    calc[2] = 1.96 * calc[1] / sqrt(size);
    return calc;
}
