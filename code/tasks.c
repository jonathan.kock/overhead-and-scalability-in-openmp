#include <stdio.h>
#include <stdlib.h>

long task_reps = -1;


void task (int reps) {
    int i;
    double a = 0.0;
    for(i = 0; i < reps; i++) {
        a += i;
    }
}

void reduction_reference(long reps) {
    int j;
    int aaaa = 0;
    for (j = 0; j < reps; j++) {
	    task(task_reps);
	    aaaa += 1;
    }
}

void atomic_reference(long reps){
    int j;
    int j_limit= reps*1000;
    double aaaa = 0.0;
    double epsilon = 1.0e-15;
    double b, c;
    b = 1.0;
    c = (1.0 + epsilon);
    for (j = 0; j < j_limit; j++) {  
	    aaaa += b;
	    b *= c;
    }
    if (aaaa < 0.0)
	printf("%f\n", aaaa);
}

/* used for hints */
void array_task (int reps,double * a) {
    int i;
    for(i = 0; i < reps; i++) {
        *a += i;
    }
}
