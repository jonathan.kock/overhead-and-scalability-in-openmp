#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <stdbool.h>

#include "core.h"
#include "print.h"
#include "benchmarks.h"
#include "thesis_math.h"
#include "tasks.h"

long reps = -1;
int iterations = -1;
char *path;
char *options;
char *name_ext;
bool bench_for = false;
bool bench_sync = false;
bool bench_task = false;
bool bench_loop = false;
bool bench_weak = false;
bool bench_strong = false;
bool bench_hints = false;


//int calctaskreps();
//long getreps(void (*)(long));

/* measures how long test takes to complete */
double* measure(void (*test)(long)) {
    int i;
    double *results = malloc(sizeof(double) * iterations); //generate runtime results array

    double start_time;

    test(reps); //do an inital unmeasured run because 1st runs tend to be considerable statisticla outlier
    for(i = 0; i < iterations; i++) {
        start_time = omp_get_wtime();
        test(reps);
        results[i] = omp_get_wtime() - start_time;
    }

    return results;
}

/* executes benchmark, tracks and prints time results */
int benchmark(char name[], void (*test)(long), bool weak) {

    double *results = measure(test);
    int i;

    print_string(name);
    print_doubles(iterations,results);
    double * math = do_math(iterations,results);
    if(weak) {
        double total[6];
        for(i = 0; i < 3; i++) {
            total[i] = math[i];
        }
        total[3] = total[0] - getsetRefMean(test,0);
        total[4] = 1.96 * (getsetRefStddev(test,0)+total[1]);
        total[5] = total[0]/getsetRefMean(test,0);
        print_doubles(iterations,results);
        print_result(6,total,name);
    } else {
        print_doubles(iterations,results);
        print_result(3,math,name);
    }
    print_string("");
    free(math);
    free(results);
    return 0;
}

/* executes benchmark, tracks and prints time results */
int reference(char name[], void (*test)(long)) {

    double *reference_results = measure(test);

    print_string(name);
    print_doubles(iterations,reference_results);
    double * math = do_math(iterations,reference_results);
    getsetRefMean(test,math[0]);
    getsetRefStddev(test,math[1]);
    print_result(3,math,name);
    print_string("");
    free(math);
    free(reference_results);
    return 0;
}

/* executes selected benchmarks*/
int execute() {
    printf("executing....\n");
    omp_set_num_threads(threads_number);
    if(bench_for || bench_loop || bench_task ||bench_sync) {
        reference("Reference for_reference",&for_reference);
    }
    if(bench_weak) {
        if(bench_for) {
            chunksize = 1;
            char name[128];
            while(chunksize <= 128) {
                snprintf(name,128,"Benchmark for_weak_static chunksize = %d",chunksize);
                benchmark(name,&for_weak_static,true);
                snprintf(name,128,"Benchmark for_weak_dynamic chunksize = %d",chunksize);
                benchmark(name,&for_weak_dynamic,true);
                chunksize *= 2;
            }
        }
        if(bench_task) {
            benchmark("Benchmark task_weak",&task_weak,true);
        }
        if(bench_loop) {
            benchmark("Benchmark task_loop_weak",&task_loop_weak,true);
        }
    }

    if(bench_strong) {
        if(bench_for) {
            chunksize = 1;
            char name[128];
            while(chunksize <= 128) {
                snprintf(name,128,"Benchmark for_strong_static chunksize = %d",chunksize);
                benchmark(name,&for_strong_static,false);
                snprintf(name,128,"Benchmark for_strong_dynamic chunksize = %d",chunksize);
                benchmark(name,&for_strong_dynamic,false);
                chunksize *= 2;
            }
        }
        if(bench_task) {
            benchmark("Benchmark task_strong",&task_strong,false);
        }
        if(bench_loop) {
            benchmark("Benchmark task_loop_strong",&task_loop_strong,false);
        }
    }
    if(bench_sync) {
        reference("Reference atomic_reference",&atomic_reference);
        reference("Reference reduction_reference",&reduction_reference);
        benchmark("Benchmark barrier_bench",&barrier_bench,true);
        benchmark("Benchmark single_bench",&single_bench,true);
        benchmark("Benchmark critical_bench",&critical_bench,true);
        benchmark("Benchmark lock_bench",&lock_bench,true);
        benchmark("Benchmark order_bench",&order_bench,true);
        benchmark("Benchmark atomic_bench",&atomic_bench,true);
        benchmark("Benchmark reduction_bench",&reduction_bench,true);
    }
    if(bench_hints) {
        char name[128];
        int i = 1;
        while(i <= 262144) {
            array_size = i;
            snprintf(name,128,"Reference array_reference arraysize = %d",i);
            reference(name,&array_reference);
            snprintf(name,128,"Benchmark n_hints arraysize = %d",i);
            benchmark(name,&n_hints,true);
            snprintf(name,128,"Benchmark u_hints arraysize = %d",i);
            benchmark(name,&u_hints,true);//comment this line if using clang, this combination does not work with it v.13
            snprintf(name,128,"Benchmark s_hints arraysize = %d",i);
            benchmark(name,&s_hints,true);
            snprintf(name,128,"Benchmark c_hints arraysize = %d",i);
            benchmark(name,&c_hints,true);
            
            snprintf(name,128,"Benchmark c_s_hints arraysize = %d",i);
            benchmark(name,&c_s_hints,true);
            snprintf(name,128,"Benchmark c_n_hints arraysize = %d",i);
            benchmark(name,&c_n_hints,true);
            snprintf(name,128,"Benchmark u_s_hints arraysize = %d",i);
            benchmark(name,&u_s_hints,true);
            snprintf(name,128,"Benchmark u_n_hints arraysize = %d",i);
            benchmark(name,&u_n_hints,true);//comment this line if using clang, this combination does not work with it v.13
            i*=4;
        }
    }
    return 0;
}

/*
int calctaskreps(void) {
    double delay_seconds = delay_time * 1e-6;
    double starttime = 0.0;
    double time = 0.0;
    int rep = 100;
    int reps = 0;
    while (time < delay_seconds) {
        reps = reps * 1.1 + 1;
        starttime = omp_get_wtime();
        for (int i = 0; i < rep; i++) {
            task(reps);
        }
        time = (omp_get_wtime() - starttime) / (double) rep;        
    }
    return reps;
    
}
long getreps(void (*test)(long)) {
    long reps = 5;
    double test_seconds = test_time * 1e-6;
    double starttime = 0.0;
    double time = 0.0;
    while (time < test_seconds) {
        reps *= 2;
        starttime = omp_get_wtime();
        test(reps);
        time = (omp_get_wtime() - starttime);  
    }
    return reps;

}
*/

int parse_args(int argc, char* argv[]) {    //check if format is correct, then set launch parameters. if correct return 0, else 1
    if(argc == 2 && strcmp(argv[1], "-help" ) == 0) {
        printf("Desired Input Format: %s <-parameters> <numberofthreads> <iterations> <nonsplit> <reps> <path> <name ext>\n",argv[0]);
        printf("Parameters: \n");
        printf("-f: for benchmark suit\n");
        printf("-t: task benchmark suit\n");
        printf("-l: task loop benchmark suit\n");
        printf("-s: synchronization benchmark suit\n");
        printf("-h: hints benchmark suit\n");
        printf("For hints benchmarks keep in mind that the repetitions will be divided by the length of the array multiplied with the amount of cores. The maximum length of the array is currently 262144.\n");
        printf("-w: weak benchmarks\n");
        printf("-r: strong benchmarks, currently disabled\n");
        printf("Numberofthreads: Integer, the number of OMP Threads to use\n");
        printf("Iterations: Integer, number of times the desired benchmarks will be performed in sequence\n");
        printf("Nonsplit: Long, length of the workload in FLOPS that will not be executed in parallel\n");
        printf("Reps: Long, times the nonsplittable workload will be executed in parallel\n");
        printf("Path: A file for the output will be generated at the desired path. A path has to be provided\n");
        printf("name ext: name extension, optional");
        exit(0);
    }
    if(!(argc == 7 || argc == 8) || strchr(argv[1],'-') != argv[1]) {//check if format is correct
        printf("Wrong parameter input, try executing with %s -help  instead\n",argv[0]);
        exit(1);
    }

    options = argv[1]; //save parameters
    if(strchr(argv[1],'f') != 0) { bench_for = true; } //select selected benchmarks
    if(strchr(argv[1],'s') != 0) { bench_sync = true; }
    if(strchr(argv[1],'t') != 0) { bench_task = true; }
    if(strchr(argv[1],'l') != 0) { bench_loop = true; }
    if(strchr(argv[1],'h') != 0) { bench_hints = true; }
    if(strchr(argv[1],'w') != 0) { bench_weak = true; }
    //if(strchr(argv[1],'r') != 0) { bench_strong = true; } disabled

    if(!(((bench_for || bench_task || bench_loop) & (bench_weak || bench_strong)) || bench_sync || bench_hints)) { //check if a benchmark is selected, else exit
        printf("No Benchmarks selected, exiting\n");
        exit(0);
    }

    threads_number = atoi(argv[2]); //get number of threads

    if(omp_get_max_threads() < threads_number) {
        printf("Not enough threads available\n");
        exit(0);
    } else {
        printf("executing with %d threads available\n", omp_get_max_threads());
    }

    iterations  = atoi(argv[3]); //get number of iterations

    task_reps = atol(argv[4]); //get delay workload

    reps = atol(argv[5]); //get repititions
    
    path = argv[6]; //get the path

    if(argc == 8) {
        name_ext = argv[7];
    } else {
        name_ext = "";
    }

    return 0;
}

int main(int argc, char* argv[]) {   //desired input format: programname -parameters numberofthreads iterations delaytime testtime outputpath
    if(parse_args(argc,argv)!=0) { //if not desired input exit
        exit(1);
    }
    //char choice;
    printf("Executing with parameters: BENCH_FOR: %s, BENCH_TASK: %s, BENCH_LOOP: %s, BENCH_SYNC: %s, BENCH_HINT: %s, BENCH_STRONG: %s, BENCH_WEAK: %s, number of threads = %d, iterations=%d, nonsplit=%li, repititions=%li path=%s, name extension:%s: "
            , bench_for ? "true":"false", bench_task ? "true":"false", bench_loop ? "true":"false", bench_sync ? "true":"false", bench_hints ? "true":"false", bench_strong ? "true":"false", bench_weak ? "true":"false", 
            threads_number, iterations, task_reps, reps, path, name_ext);
    if (print_setup(path, options, threads_number, iterations, task_reps, reps, name_ext) == 0) {
        execute();
    }
    /*
    scanf(" %c", &choice);
    while (choice != 'n' && choice != 'y') {
        printf("Please enter [y/n]: ");
        scanf(" %c", &choice);
    }
    
    if(choice == 'n') {
        printf("Exiting\n");
        exit(0);
    } else if(choice == 'y') {
        if (print_setup(path, options, threads_number, iterations, task_reps, reps) == 0) {
            execute();
        }
    } 
    */
    print_close();
    exit(0);
}
