#include <stdio.h>
#include <stdlib.h>

#include "benchmarks.h"
#include "tasks.h"

int threads_number = -1;
int info = -1;
int chunksize = 1;
double task_mean = -1;
double task_stddev = -1;
double atomic_mean = -1;
double atomic_stddev = -1;
double reduction_mean = -1;
double reduction_stddev = -1;
double hints_mean = -1;
double hints_stddev = -1;
omp_lock_t lock;
int array_size = 0; //the array size for hints test to vary the size of the array for higher and lower contention levels

/*returns the mean of the reference fitting the test, if mean unequal to 0 sets mean*/
double getsetRefMean(void (*test) (long),double mean) {
    if (test == &atomic_reference || test == &atomic_bench) {
        if(mean != 0) {
            atomic_mean = mean;
        }
        return atomic_mean;  
    } else if (test == &reduction_reference || test == &reduction_bench) {
        if(mean != 0) {
            reduction_mean = mean;
        }
        return reduction_mean;    
    } else if (test == &array_reference || test == &n_hints || test == &u_hints || test == &c_hints || test == &s_hints || test == &c_n_hints || test == &c_s_hints || test == &u_n_hints || test == &u_s_hints) {
        if(mean != 0) {
            hints_mean = mean;
        }
        return hints_mean;               
    } else {
        if(mean != 0) {
            task_mean = mean;
        }
        return task_mean;
    }
}

/*returns the stddev of the reference fitting the test, if stddev unequal to 0 sets mean*/
double getsetRefStddev(void (*test) (long),double stddev) {
    if (test == &atomic_reference || test == &atomic_bench) {
        if(stddev != 0) {
            atomic_stddev = stddev;
        }
        return atomic_stddev;  
    } else if (test == &reduction_reference || test == &reduction_bench) {
        if(stddev != 0) {
            reduction_stddev = stddev;
        }
        return reduction_stddev;   
    } else if (test == &array_reference || test == &n_hints || test == &u_hints || test == &c_hints || test == &s_hints || test == &c_n_hints || test == &c_s_hints || test == &u_n_hints || test == &u_s_hints) {
        if(stddev != 0) {
            hints_stddev = stddev;
        }
        return hints_stddev;   
    } else {
        if(stddev != 0) {
            task_stddev = stddev;
        }
        return task_stddev;
    }
}


/* reference task for task */
void reference_task (long reps) {
    long i;
    for(i = 0; i < reps; i++) {
        task(task_reps);
    }
}

void for_reference(long reps) {
    long i;
    for(i = 0; i < reps; i++) {
        task(task_reps);
    }
}

void for_weak_static(long reps) {
    long i;
    long i_limit = reps * threads_number;   //to keep problem size constant per thread
    #pragma omp parallel
    {
        #pragma omp for schedule(static,chunksize)
        for(i = 0; i < i_limit; i++) {
            task(task_reps);
        }
    }
}

void for_strong_static(long reps) {
    long i;
    #pragma omp parallel
    {
        #pragma omp for schedule(static,chunksize) 
        for(i = 0; i < reps; i++) {
            task(task_reps);
        }
    }
}

void for_weak_dynamic(long reps) {
    long i;
    long i_limit = reps * threads_number;   //to keep problem size constant per thread
    #pragma omp parallel
    {
        #pragma omp for schedule(dynamic,chunksize)
        for(i = 0; i < i_limit; i++) {
            task(task_reps);
        }
    }
}

void for_strong_dynamic(long reps) {
    long i;
    #pragma omp parallel
    {
        #pragma omp for schedule(dynamic,chunksize) 
        for(i = 0; i < reps; i++) {
            task(task_reps);
        }
    }
}

void task_weak(long reps) {
    long i;
    long i_limit = reps * threads_number;   //to keep problem size constant per thread
    #pragma omp parallel
    {
        #pragma omp for schedule(static) 
        for(i = 0; i < i_limit; i++) {
            #pragma omp task
            { task(task_reps); }
        }
    }
}

void task_strong(long reps) {
    long i;
    #pragma omp parallel
    {
        for(i = 0; i < reps; i++) {
            #pragma omp task
            { task(task_reps); }
        }
        #pragma omp taskwait
    }    
}

void task_loop_weak(long reps) {
    long i;
    #pragma omp parallel
    {
        #pragma omp taskloop
        for(i = 0; i < reps; i++) {
            task(task_reps);
        }
    }
}

void task_loop_strong(long reps) {
    long i;
    #pragma omp parallel
    {
        #pragma omp taskloop
        for(i = 0; i < reps; i++) {
            task(task_reps);
        }
    }
}

/*
start of sync benchmarks and hint benchmarks
uses the same workload as the respective reference task, but is using a synchronization construct and splits the workload as possible via threads
*/


/* reference task for array_task */
void array_reference(long reps) {
    if(array_size == 0) { return; }
    long reps_limit = reps / (array_size);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long i;
    int j;
    for(i = 0; i < reps_limit; i++) {
        for(j = 0; j < array_size; j++) {
            array_task(task_reps,a+j);
        }
    }
    free(a);
}

void barrier_bench(long reps) {
    long j;
    #pragma omp parallel private(j)
    {
	    for (j = 0; j < reps; j++) {
	        task(task_reps);
            #pragma omp barrier
	    }
    }
}

void single_bench(long reps) {
    long j;
    #pragma omp parallel private(j)
    {
	    for (j = 0; j < reps; j++) {
            #pragma omp single
	            task(task_reps);
	    }
    }
}

void critical_bench(long reps) {
    long j;
    #pragma omp parallel private(j)
    {
	    for (j = 0; j < reps/threads_number; j++) {
            #pragma omp critical
	        {
		        task(task_reps);
	        }
	    }
    }
}

void lock_bench(long reps) {
    long j;

    #pragma omp parallel private(j)
    {
	    for (j = 0; j < reps/threads_number; j++) {
	        omp_set_lock(&lock);
            task(task_reps);
	        omp_unset_lock(&lock);
	    }
    }
}

void order_bench(long reps) {
    long j;
    #pragma omp parallel for ordered schedule (static,1)
        for (j = 0; j < reps; j++) {
            #pragma omp ordered
                task(task_reps);
        }
}

void atomic_bench(long reps) {
    int j;
    int j_limit= reps/threads_number*1000;
    double aaaa = 0.0;
    double epsilon = 1.0e-15;
    double b,c;
    b = 1.0;
    c = (1.0 + epsilon);
    #pragma omp parallel private(j) firstprivate(b)
    {
	    for (j = 0; j < j_limit; j++) {
            #pragma omp atomic	
	            aaaa += b;
	            b *= c;
	    }
    }
}

void reduction_bench(long reps) {
    int j;
    int aaaa = 0;
    for (j = 0; j < reps; j++) {
    #pragma omp parallel reduction(+:aaaa)
	{
	    task(task_reps);
	    aaaa += 1;
	}
    }
}

//hints

/*nonspeculative*/
void n_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (ncritical) hint(omp_lock_hint_nonspeculative)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*uncontended
IMPORTANT: DOES NOT WORK WITH CLANG, disable in core
*/
void u_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (ucritical) hint(omp_lock_hint_uncontended)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*speculative*/
void s_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (scritical) hint(omp_lock_hint_speculative)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*contended*/
void c_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (ccritical) hint(omp_lock_hint_contended)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*uncontended, nonspeculative 
IMPORTANT: DOES NOT WORK WITH CLANG, disable in core
*/
void u_n_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (uncritical) hint(omp_lock_hint_uncontended + omp_lock_hint_nonspeculative)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*contended, nonspeculative*/
void c_n_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (cncritical) hint(omp_lock_hint_contended + omp_lock_hint_nonspeculative)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*uncontended, speculative*/
void u_s_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (uscritical) hint(omp_lock_hint_uncontended + omp_lock_hint_speculative)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}

/*contended, speculative*/
void c_s_hints(long reps) {
    if(array_size == 0) {return;}
    long reps_limit = reps / (array_size*threads_number);
    if(reps_limit < 1) { return ; }
    double * a = malloc(sizeof(double) * array_size);
    long j;
    int i;
    #pragma omp parallel private(i,j) shared(a)
    {
	    for (j = 0; j < reps_limit; j++) {
            for (i = 0; i < array_size; i++) {
                #pragma omp critical (cscritical) hint(omp_lock_hint_contended + omp_lock_hint_speculative)
	            {
		            array_task(task_reps,a+i);
	            }
            }
	    }
    }
    free(a);
}
