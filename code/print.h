//to output data into a csv file
#include <stdio.h>
#include <time.h>


int print_setup(char *, char *, int, int, long, long, char *);
int print_int(int , int *);
int print_string(char *);
int print_doubles(int, double *);
int print_result(int, double *, char *);
int print_close(void);
